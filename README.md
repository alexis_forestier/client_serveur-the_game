# Client_Serveur-THE_GAME

## INTRODUCTION
The objective of this project is the realization of a Client-Server application allowing to play “The Game” either alone or against one or more other players.

## SOFTWARE
### 1. SERVER
- OS : Linux

### 2. CLIENTS
- Technologies : C++ or Python
- OS : Linux (for C++ client), Linux & Windows (for Python client)

Please do not use windows cmd terminal !

Windows terminal recommendations : 
- Powershell Terminal
- [Windows Terminal](https://www.microsoft.com/fr-fr/p/windows-terminal/9n0dx20hk701?activetab=pivot:overviewtab)

## REQUIREMENT
Server & Client C++ (Linux)
- g++
- qmake

Python (Linux & Windows)
- [Python 3.10.1+](https://www.python.org/downloads/)

## LAUNCH SERVER & CLIENT

### Server
In the Server folder :
```
./server <optional_port>
```

### C++ client
In the Clients/Cpp folder :
```
./client <ip_address> <port>
```

### Python client
In the Clients/Python UI folder :
```
python3 game.py
```

## COMMANDS

### 1. In the lobby or in a room

- `ROOM <room_name>` create a room
- `JOIN <room_name>` join a room already created
- `USER <user_name>` change user name
- `STAT` display user information
- `SERV` display server information
    - `SERV -ul` display users list
    - `SERV -rl` display rooms list
- `HELP` display help
- `QUIT` quit the room
    - `QUIT -s` quit the server   
### 2. Only in a room
- `PLAY` start the game
- `SERV -rul` display users list in the room

### 3. In game
- `CARD <card_position> <pile_number>` play cards
- `CONF` confirm your turn
- `INFO` display game information
    - `INFO -l` display user list in this room
- `HELP` display help
- `QUIT` quit the room



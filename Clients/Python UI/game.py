from os import popen
import time
import tkinter
import socket
import threading

from tkinter import *
from tkinter import messagebox
from turtle import color

HOST = '127.0.0.1'
PORT = 3490
BOULE = True
listRoom = []
listUser = []
user = "test"

#SCREEN

def createScreenLobby() :
    labelName.pack()
    myLabel.pack()
    roomName.pack()
    buttonCreateRoom.pack()
    listRoomBox.pack()
    buttonrefresh.pack()
    buttonJoin.pack()
    userPseudo.pack()
    buttonChangePseudo.pack()
    buttonLeaveApp.pack()
    refreshRooms()
    getUserName()

def deleteScreenLobby():
    myLabel.pack_forget()
    roomName.pack_forget()
    buttonCreateRoom.pack_forget()
    listRoomBox.pack_forget()
    buttonrefresh.pack_forget()
    buttonJoin.pack_forget()
    userPseudo.pack_forget()
    buttonChangePseudo.pack_forget()
    buttonLeaveApp.pack_forget()

def createScreenRoom() :
    listUserBox.pack()
    buttonStartGame.pack()
    buttonLeaveRoom.pack()
    refreshUsers()

def deleteScreenRoom():
    listUserBox.pack_forget()
    buttonLeaveRoom.pack_forget()
    buttonStartGame.pack_forget()

#def deleteScreenGame():
    


#FUNCTION

def createRoom():
    if roomName.get() in listRoom :
        messagebox.showwarning("Warning", "This room already existing")
        return

    if roomName.get() == '' :
        messagebox.showwarning("Warning", "The name of the Room is empty")
        return

    commandROOM = "ROOM "
    commandROOM += roomName.get()
    commandROOM += "n"
    n = client.send(commandROOM.encode())
    if (n != len(commandROOM)):
        print('Send error')
        return

    donnees = client.recv(1024)    
    print(donnees.decode("utf-8")) #return server
    listRoom.append(roomName.get())
    listRoomBox.insert(END, roomName.get())
    roomName.delete(0, END)
    deleteScreenLobby()
    createScreenRoom()

def joinRoom():
    commandJOIN = "JOIN "
    commandJOIN += listRoomBox.get(ANCHOR)
    commandJOIN += "n"
    n= client.send(commandJOIN.encode())
    if (n != len(commandJOIN)):
        print('Send error')

    donnees = client.recv(1024)    
    print(donnees.decode("utf-8")) #return server
    deleteScreenLobby()
    createScreenRoom()

def changePseudo():
    global user
    if userPseudo.get() == user:
        messagebox.showwarning("Warning", "The new pseudo is the same")
        userPseudo.delete(0, END)
        return

    if userPseudo.get() == '' :
        messagebox.showwarning("Warning", "The new pseudo is empty")
        return

    if " " in userPseudo.get() :
        messagebox.showwarning("Warning", "Your pseudo can't contains space")
        userPseudo.delete(0, END)
        return

    commandUSER = "USER "
    commandUSER += userPseudo.get()
    commandUSER += "n"
    n = client.send(commandUSER.encode())
    if (n != len(commandUSER)):
        print('Send error')
        return

    donnees = client.recv(1024)    
    print(donnees.decode("utf-8")) #return server
    user = userPseudo.get()
    userPseudo.delete(0, END)
    labelName.configure(text=user)

def refreshRooms():
    commandSERV = "SERV -rl"
    n= client.send(commandSERV.encode())
    if (n != len(commandSERV)):
        print('Send error')

    donnees = client.recv(1024)
    getListRoom(donnees.decode("utf-8"))

def refreshUsers(): 
    commandSERV = "SERV -rul"
    n= client.send(commandSERV.encode())
    if (n != len(commandSERV)):
        print('Send error')

    donnees = client.recv(1024)
    print(donnees.decode("utf-8")) #return server
    getListUser(donnees.decode("utf-8"))

def leaveApp():
    commandQUIT = "QUIT -s"
    n= client.send(commandQUIT.encode())
    if (n != len(commandQUIT)):
        print('Send error')

    donnees = client.recv(1024)    
    print(donnees.decode("utf-8")) #return server
    client.close()
    exit()

def leaveRoom():
    commandQUIT = "QUIT"
    n= client.send(commandQUIT.encode())
    if (n != len(commandQUIT)):
        print('Send error')

    donnees = client.recv(1024)    
    print(donnees.decode("utf-8")) #return server
    deleteScreenRoom()
    createScreenLobby()
    refreshRooms()

def startGame() :
    #Zbi
    print("game start")
    
def getListRoom(data) :
    listRoom.clear()
    listRoomBox.delete(0, END)
    temp = data.split("- ")
    i = 1
    while i < len(temp) - 1:
        listRoom.append(temp[i].split("\n")[0])
        listRoomBox.insert(END, temp[i].split("\n")[0])
        i = i + 1

    test = temp[i].split("\n")
    listRoom.append(test[0])
    listRoomBox.insert(END, test[0])

def getListUser(data) : 
    listUser.clear()
    listUserBox.delete(0, END)
    temp = data.split("- ")
    i = 1
    while i < len(temp) - 1:
        listUser.append(temp[i].split("\n")[0])
        listUserBox.insert(END, temp[i].split("\n")[0])
        i = i + 1

    test = temp[i].split("\n")
    listUser.append(test[0])
    listUserBox.insert(END, test[0])

def getUserName():
    global user
    commandSTAT = "STAT"
    n= client.send(commandSTAT.encode())
    if (n != len(commandSTAT)):
        print('Send error')

    donnees = client.recv(1024)    
    print(donnees.decode("utf-8")) #return server
    data = donnees.decode("utf-8")
    temp = data.split(" : ")
    user = temp[2].split("\n")[0]
    labelName.configure(text=user)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((HOST, PORT))
print('Connexion vers ' + HOST + ':' + str(PORT) + ' reussie.')

root = Tk()
root.title("THE GAME")
root.geometry("600x600")
root.configure(background='red')

labelName = Label(root, text=user, font=("Helvetica", 20,), background='red')
myLabel = Label(root, text="Lobby", font=("Helvetica", 20,), background='red')
roomName = Entry(root)
buttonCreateRoom = Button(root, text="Create room", command=createRoom)
listRoomBox = Listbox(root)
buttonrefresh = Button(root, text='Refresh', command=refreshRooms)
buttonJoin = Button(root, text='Join room', command=joinRoom)
userPseudo = Entry(root)
buttonChangePseudo = Button(root, text="Change pseudo", command=changePseudo)
buttonLeaveApp = Button(root, text='Leave App', command=leaveApp)

listUserBox = Listbox(root)
buttonStartGame = Button(root, text="Start game", command=startGame)
buttonLeaveRoom = Button(root, text='Leave room', command=leaveRoom)

createScreenLobby()

root.mainloop()
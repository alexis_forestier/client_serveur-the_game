import socket
import threading

HOST = '127.0.0.1'
PORT = 3490

def write():
    input()

def read():
    donnees = client.recv(1024)
    print(donnees.decode("utf-8"))


client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((HOST, PORT))
print('Connexion vers ' + HOST + ':' + str(PORT) + ' reussie.')
message = input()
print(type(message.encode()))
print('Envoi de :' + message)
n = client.send(message.encode())
if (n != len(message)):
        print('Erreur envoi.')
else:
        print('Envoi ok.')


t1 = threading.Thread(target=read)

t1.start()

t1.join()

print('Deconnexion.')
client.close()
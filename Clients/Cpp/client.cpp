
#include <sys/socket.h>
#include <unistd.h>
#include <iostream>
#include <thread>
#include <csignal>
#include <cstdlib>

#include <sys/time.h>
#include "../../Class/socket.h"
#include "../../Class/request.h"

using namespace std;
using namespace stdsock;

bool run = true;
StreamSocket *sock;

void signalHandler(__attribute__((unused)) const int signum)
{
    exit(EXIT_FAILURE);
}
void driverEpilog()
{
    const char *str = string("QUIT -s").data();
    if (write(sock->getSockfd(), str, strlen(str)) <= 0)
    {
        cout << "Writting error" << endl;
    }
}

int main(int argc, char *argv[])
{
    Request input, output;
    std::thread threads[2];
    int port;

    // DISCONNECT FUNCTION
    signal(SIGTERM, signalHandler);
    signal(SIGINT, signalHandler);
    atexit(driverEpilog);

    // argv[0] = command line (ex: telnet, ./Client...)
    // argv[1] = address ip (ex: 127.0.0.1)
    // argv[2] = port (ex: 3490)

    if (argc != 3)
    {
        printf("usage: %s server_address port\n", argv[0]);
        return 0;
    }

    if (sscanf(argv[2], "%d", &port) != 1)
    {
        printf("usage: %s server_address port\n", argv[0]);
        return 1;
    }

    //
    cout << "Attemping to reach " << argv[1] << ":" << port << endl;
    sock = new StreamSocket(std::string(argv[1]), port);

    int err = sock->connect();
    if (err != 0)
    {
        delete sock;
        perror("[-]Error in connection: ");
        return (err);
    }
    cout << "[+]Connected to Server.\n";

    input.setReader(0);
    input.setWriter(sock->getSockfd());
    threads[0] = std::thread(doRequest, std::ref(input), std::ref(run));

    output.setReader(sock->getSockfd());
    output.setWriter(1);
    threads[1] = std::thread(doRequest, std::ref(output), std::ref(run));

    //
    threads[0].join();
    threads[1].join();

    delete sock;
    cout << "stop\n";

    return 0;
}

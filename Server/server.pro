TEMPLATE = app

CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG+=thread
LIBS+=-pthread

INCLUDEPATH += ../

SOURCES += \
	server.cpp \
	../Class/socket.cpp \
	../Class/request_server.cpp \
	../Class/Model/User.cpp \
	../Class/Model/Room.cpp \
	../Class/Model/Game.cpp \
	../Class/Model/Lobby.cpp \
	Protocol/protocol.cpp \
	

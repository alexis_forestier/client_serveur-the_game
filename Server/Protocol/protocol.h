#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <iostream>
#include <unistd.h>
#include <string.h>
#include <deque>
#include <regex>

using namespace std;

class Lobby;
class User;
class Room;


void protocol(char *request_buffer, int sock_fd, Lobby* lobby, User* user);
void gameProtocol(char *request_buffer, int sock_fd, Room *room, deque<int> &cards, bool &conf, int &nb_pose);

#endif //PROTOCOL_H
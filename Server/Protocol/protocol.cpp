

#include "protocol.h"

#include <string>

#include "../../Class/Model/Lobby.h"
#include "../../Class/Model/User.h"
#include "../../Class/Model/Room.h"

using namespace std;

const regex chr_only("^[a-zA-Z0-9_\\-\\.]+.$", regex_constants::extended);
smatch stringmatcher;

const string _PREFIX_ = "\u001b[34;1m[Server]\u001b[0m ";

/**
 * @brief
 * key words use for requests
 */
const char *KEY_WORDS[] = {
    "ROOM", //0 create a room
    "JOIN", //1 join a room already created
    "USER", //2 change user name
    "PLAY", //3 start the game
    "CARD", //4 play cards
    "CONF", //5 confirm your turn
    "QUIT", //6 quit the room (or the server with -s as param1)
    "STAT", //7 display user informations
    "HELP", //8 display help
    "INFO",  //9 display game informations
    "SERV"  //10 display server informations
};

void protocol(char *request_buffer, int sock_fd, Lobby *lobby, User *user)
{
    deque<string> request_message;
    string response = "\u001b[31mThis request doesn't exist, use HELP command\u001b[0m";

    // split the client request
    char *rb_split = strtok(request_buffer, " ");
    while (rb_split != NULL)
    {
        printf("command part : %s\n", rb_split);
        request_message.push_back(rb_split);
        rb_split = strtok(NULL, " ");
    }

    // handle the "\0" character
    if (request_message[0].length() == 5)
    {
        request_message[0] = request_message[0].substr(0, 4);
    }

    // All command cases
    if (request_message[0].length() == 4)
    {

        // ROOM
        /**
         * @brief
         * Create a room
         *
         * @param room_name
         * Cannot be called "lobby"
         * Cannot create room if user is already in a room
         */
        if (request_message[0].compare(0, 4, KEY_WORDS[0]) == 0)
        {

            // param1 = room_name
            if (regex_match(request_message[1], stringmatcher, chr_only))
            {
                string roomName = request_message[1].substr(0, request_message[1].length() - 1);

                if (lobby->addRoom(roomName, user) == true)
                {

                    response = "\u001b[32mThe room \"" + roomName + "\" has been created !\u001b[0m";
                }
                else
                {
                    response = "\u001b[31mThe room \"" + roomName + "\" has not been created!\u001b[0m";
                }
                request_message[1] = "";
            }
            else
            {
                response = "\u001b[31mWrong parameter, ROOM <room_name>\u001b[0m";
            }
        }

        // JOIN
        /**
         * @brief
         * Join a room already created
         *
         * @param room_name
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[1]) == 0)
        {

            // param1 = room_name
            if (regex_match(request_message[1], stringmatcher, chr_only))
            {
                string roomName = request_message[1].substr(0, request_message[1].length() - 1);
                Room *room = lobby->findRoomByName(roomName);

                if (room != nullptr && user->getRoomID() == 0 && room->isPlaying() == false)
                {
                    room->addUser(user);

                    response = "\u001b[32mYou have joined the room \"" + request_message[1].substr(0, request_message[1].length() - 1) + "\" !\u001b[0m";
                }
                else
                {
                    response = "\u001b[31mThe room \"" + request_message[1].substr(0, request_message[1].length() - 1) + "\" doesn't exist !\u001b[0m";
                }

                request_message[1] = "";
            }
            else
            {
                response = "\u001b[31Wrong parameter, JOIN <room_name>\u001b[0m";
            }
        }

        // USER
        /**
         * @brief
         * Change user name
         *
         * @param user_name
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[2]) == 0)
        {

            // param1 = user_name
            if (regex_match(request_message[1], stringmatcher, chr_only))
            {
                string userName = request_message[1].substr(0, request_message[1].length() - 1);

                user->setUserName(userName);
                response = "\u001b[32mYou identified yourself as \"" + userName + "\" !\u001b[0m";
            }
            else
            {
                response = "\u001b[31mWrong parameter, USER <user_name>\u001b[0m";
            }
            request_message[1] = "";
        }

        // PLAY
        /**
         * @brief
         * Start the game
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[3]) == 0)
        {
            Room *room = user->getRoom();
            if(room != nullptr){
                room->play();
                response = "";
            }
            else{
                response = "\u001b[31mYou are not in a room\u001b[0m";
            }
            
        }

        // CARD
        /**
         * @brief
         * play cards
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[4]) == 0)
        {
            // See gameProtocol below
            response = "";
        }

        // CONF
        /**
         * @brief
         * Confirm your turn
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[5]) == 0)
        {
            // See gameProtocol below
            response = "";
        }

        // QUIT
        /**
         * @brief
         * Quit the room
         *
         * @param -s
         * quit the server
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[6]) == 0)
        {
            if (request_message[1].compare(0, 2, "-s") == 0)
            {
                lobby->disconnectUser(user->getID());
                response = "logout";
            }
            else
            {
                Room *room = user->getRoom();
                if (room != nullptr)
                {
                    room->disconnect(user);
                    
                    response = "\u001b[33mYou have left the \"" + room->getName() + "\" room!\u001b[0m";

                    if (room->size() == 0)
                    {
                        lobby->removeRoom(room);
                    }
                }
            }
        }

        // STAT
        /**
         * @brief
         * Display user informations
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[7]) == 0)
        {
            response = "\u001b[33;1mSTAT\u001b[0m\n";
            response += "\u001b[36m------------\u001b[0m\n";
            response += "\u001b[38;5;209mYOUR ID \u001b[0m : " + user->getStringID() + "\n";
            response += "\u001b[38;5;209mYOUR NAME \u001b[0m : " + user->getUserName() + "\n";
            response += "\u001b[38;5;209mCURRENT ROOM \u001b[0m : " + user->getRoomNameEffect() + "\n";
            response += "\u001b[36m------------\u001b[0m\n";
        }

        // HELP
        /**
         * @brief
         * Display help
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[8]) == 0)
        {
            response = "\u001b[33;1mHELP\u001b[0m\n";
            response += "\u001b[36m------------\u001b[0m\n";
            response += "\u001b[38;5;209mROOM <room_name>\u001b[0m : Create a room (only in the lobby)\n";
            response += "\u001b[38;5;209mJOIN <room_name>\u001b[0m : Join a room already create (only in the lobby)\n";
            response += "\u001b[38;5;209mUSER <user_name>\u001b[0m : Identify command\n";
            response += "\u001b[38;5;209mPLAY\u001b[0m : Start the game (only in a room)\n";
            response += "\u001b[38;5;209mCARD <pos_card> <num_pile>\u001b[0m : Play a card (only in a room)\n";
            response += "\u001b[38;5;209mCONF\u001b[0m : Confirm your turn (only in a room)\n";
            response += "\u001b[38;5;209mSTAT\u001b[0m : Display user informations\n";
            response += "\u001b[38;5;209mQUIT\u001b[0m : Quit the room (add -s parameter to quit the server)\n";
            response += "\u001b[36m------------\u001b[0m";
        }

        // SERV
        /**
         * @brief
         * Display server informations
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[10]) == 0)
        {
            response = "\u001b[33;1mSERVER INFO\u001b[0m\n";
            response += "\u001b[36m------------\u001b[0m\n";
            Room* room = user->getRoom();

            if(request_message[1].compare(0, 3, "-rl") == 0){
                
                response += lobby->getRoomListToString();
            }
            else if(request_message[1].compare(0, 3, "-ul") == 0){
                
                response += lobby->getUserListToString();
            }
            else if(room != nullptr && request_message[1].compare(0, 4, "-rul") == 0){
                
                response += room->getUserListToString();
            }
            else{
                response += "\u001b[38;5;209mIP ADDRESS\u001b[0m : " + lobby->getIP() + "\n";
                response += "\u001b[38;5;209mPORT\u001b[0m : " + to_string(lobby->getPort()) + "\n";
                response += "\u001b[38;5;209mUSERS CONNECT\u001b[0m : "+to_string(lobby->size())+"\n";
                response += "\u001b[38;5;209mROOMS \u001b[0m : "+to_string(lobby->roomListSize())+"\n";
            }

            response += "\u001b[36m------------\u001b[0m";
        }
    }

    if (response.compare("") != 0)
    {
        request_message[0] = "";
        request_message.clear();

        string str = _PREFIX_ + response + "\n";
        const char *result = str.data();

        // Returns a response to the client
        if (write(sock_fd, result, strlen(result)) <= 0)
        {
            printf("Server Response Failed");
        }
    }
}



/**
 * @brief 
 * Commands allows in Game
 * @param request_buffer 
 * @param sock_fd 
 * @param room 
 * @param cards 
 * @param conf 
 */
void gameProtocol(char *request_buffer, int sock_fd, Room *room, deque<int> &cards, bool &conf, int &nb_pose)
{
    const string _PREFIX_ROOM_ = "\u001b[35m["+room->getName()+"]\u001b[0m";
    deque<string> request_message;
    string response = "\u001b[31mThis request doesn't exist, use HELP command\u001b[0m";

    // split the client request
    char *rb_split = strtok(request_buffer, " ");
    while (rb_split != NULL)
    {
        printf("command part : %s\n", rb_split);
        request_message.push_back(rb_split);
        rb_split = strtok(NULL, " ");
    }

    // handle the "\0" character
    if (request_message[0].length() == 5)
    {
        request_message[0] = request_message[0].substr(0, 4);
    }

    // All command cases
    if (request_message[0].length() == 4)
    {

        // CARD
        /**
         * @brief
         * play cards
         *
         * @param card_position
         * @param pile_number
         */
        if (request_message[0].compare(0, 4, KEY_WORDS[4]) == 0)
        {
            
            int card_pos = stoi(request_message[1]);
            int pile_number = stoi(request_message[2].substr(0, request_message[2].length() - 1));
            if (card_pos < 0 || pile_number < 1 || pile_number > 4)
            {
                response = "\u001b[31mWrong parameter, CARD <pos_card> <num_pile>\u001b[0m";
            }
            else
            {
                if (room->setPile(cards, card_pos, pile_number) == 1)
                {
                    nb_pose++;
                    response = "\u001b[33m Your cards pile : \u001b[0m" + room->cardsToString(cards);
                    response += "\n\u001b[33mPlace another card...\u001b[0m";
                }
                else
                {
                    response = "\u001b[31mInvalid card or pile number\u001b[0m";
                }
            }
            request_message[1] = "";
            request_message[2] = "";
        }

        // CONF
        /**
         * @brief
         * Confirm your turn
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[5]) == 0)
        {
            conf = 1;
            response = "You end your turn";
        }

        // INFO
        /**
         * @brief
         * Display Game informations
         * 
         * @param -l
         * Display user list in this room
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[9]) == 0)
        {
            response = "\u001b[33;1m GAME INFO\u001b[0m\n";
            response += "\u001b[36m------------\u001b[0m\n";

            if(request_message[1].compare(0, 2, "-l") == 0){
                response = room->getUserListToString();
                request_message[1] = "";
            }
            else{
                
                response += room->getInfo();
            }

            response += "\u001b[36m------------\u001b[0m";
        }

        // HELP
        /**
         * @brief
         * Display Game help
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[8]) == 0)
        {
            response = "\u001b[33;1m HELP\u001b[0m\n";
            response += "\u001b[36m------------\u001b[0m\n";
            response += "\u001b[38;5;209mCARD <pos_card> <num_pile>\u001b[0m : Play a card\n";
            response += "\u001b[38;5;209mCONF\u001b[0m : Confirm your turn\n";
            response += "\u001b[38;5;209mINFO\u001b[0m : Display game informations\n";
            response += "\u001b[38;5;209mQUIT\u001b[0m : Quit the room\n";
            response += "\u001b[36m------------\u001b[0m";
        }

        // QUIT
        /**
         * @brief
         * Quit the room
         */
        else if (request_message[0].compare(0, 4, KEY_WORDS[6]) == 0)
        {
            //TODO
        }
    }
    
    request_message[0] = "";
    request_message.clear();

    string str = _PREFIX_ROOM_ + response + "\n";
    const char *result = str.data();

    // Returns a response to the client
    if (write(sock_fd, result, strlen(result)) <= 0)
    {
        printf("Server Response Failed");
    }
}
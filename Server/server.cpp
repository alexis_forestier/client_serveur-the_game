#include <iostream>
#include <thread>
#include <deque>

#include <sys/time.h>
#include "../Class/socket.h"
#include "../Class/request.h"
#include "../Class/Model/User.h"
#include "../Class/Model/Lobby.h"

using namespace std;
using namespace stdsock;


bool working = true;
int nb_client = 1;

int main(int argc, char *argv[])
{
    /*---INIT---*/
    int port;
    Lobby* lobby;

    
    if(argc!=2 || sscanf(argv[1], "%d", &port)!=1)
    {
            printf("usage: %s port\n", argv[0]);
            // default port, if none provided
            port= 3490;
    }

    ConnectionPoint *server=new ConnectionPoint(port);
    int err= server->init();
    if (err != 0) {
        std::cout << strerror(err) << std::endl;
        exit(err);
    }
    

    //Lobby
    lobby = new Lobby(server->getIP(),server->getPort());

    StreamSocket *ssLobby=server->accept();
    
    lobby->getCom().setReader(ssLobby->getSockfd());

    

    std::thread t(
        launchLobby,
        lobby,
        ssLobby
    );

    

    cout << "Waiting clients on port " << port << " ..." << endl;

    // accepting connexion
    // and preparing communication points
    while(true){
        StreamSocket *client=server->accept();
        lobby->addUser(
            new User(client->getSockfd(),lobby->getReader(), nb_client, lobby)
        );
        cout << "[+] Client"<< nb_client << " has logged in" << endl;
        nb_client++;
    }


    /*---STOP---*/
    working = false;
    
    // waiting end of communication

    // closing connexion point
    delete server;
    cout << "stop\n";
    return 0;
}

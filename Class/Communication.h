#ifndef COM_H
#define COM_H

#include <iostream>
#include <deque>

#include <sys/time.h>

using namespace std;
using namespace stdsock;

class Communication
{
private:
    int readfd;
    int writefd;
    deque<int> writefdList;
    
public:
    Communication(){}
    Communication(Communication &com){
        this->readfd = com.getReader();
        this->writefd = com.getWriter();
        this->writefdList = com.getWriterList();
    }
    void set(int readfd, int writefd){
        this->readfd = readfd;
        this->writefd = writefd;
    }
    int getReader() const { return readfd;}
    int getWriter() const { return writefd;}
    deque<int> getWriterList() const { return writefdList;}
    void setReader(int sockfd) {
        readfd=sockfd;
    }
    void setWriter(int sockfd) {
        writefd=sockfd;
    }
    void addWriter(int sockfd){
        writefdList.push_back(sockfd);
    }
    void removeWriter(int i){
        writefdList.erase(writefdList.begin()+i);
    }
    int getWriterBySock(int i){
        return writefdList[i];
    }
    int size(){
        return writefdList.size();
    }
    ~Communication(){};
};

#endif // COM_H
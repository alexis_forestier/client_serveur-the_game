#include "User.h"

#include "Room.h"

using namespace std;

User::User(int reader, int writer, int id, Lobby *lobby)
{
    this->reader = reader;
    this->writer = writer;
    this->id = id;

    this->room = nullptr;
    this->room_id = 0;
    this->room_name = "Lobby";

    this->user_name = "Client" + to_string(id);
    r_user.set(reader, writer);

    run = true;
    t = std::thread(request, std::ref(r_user), this, lobby);
}
#ifndef __GAME_H__
#define __GAME_H__

#include <iostream>
#include <thread>
#include <deque>
#include <stdlib.h>
#include <time.h>

using namespace std;

class User;
class Room;

typedef struct
{
    User* player;
    deque<int> cards;
    bool lose;
} Player;



class Game
{
private:
    deque<User *> players;
    Room *room;

    deque<int> cardsPile;

public:
    int pile1, pile2, pile3, pile4;
    int round;
    int nb_cardByPlayer;

    Game(deque<User *> players_list, Room *room);

    Room* getRoom(){return this->room;}

    void initCardsPile();

    deque<int> getCards(int n);

    int cardsPileSize(){return this->cardsPile.size();}

    void pickCards(deque<int> &player_cards);

    deque<Player> initPlayers();

    string getPileToString(deque<int> &player_cards);

    void sendData(deque<Player> players);

    /**
     * @brief 
     * 
     * @param players 
     * @return int 
     * 0: false, 1:players win, 2:game win
     */
    int gameover(deque<Player> players);

    bool playerLose(Player player);

    ~Game(){}
};
void playGame(Game *game);


#endif // __GAME_H__
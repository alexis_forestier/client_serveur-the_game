
#include <string.h>

#include "User.h"
#include "Room.h"

#include "Lobby.h"

Lobby::Lobby(string ip, int port)
{
    this->ip = ip;
    this->port = port;

    this->t = new std::thread(socketConnect, this, ip, port);
}

void Lobby::addUser(User *user)
{
    this->com_server.addWriter(user->getTalk().getReader());
    this->userList.push_back(user);
};

void Lobby::removeUser(int i)
{
    this->com_server.removeWriter(i);
    User *tmp = this->userList[i];
    this->userList.erase(this->userList.begin() + i);
    delete tmp;
};

bool Lobby::addRoom(string name, User *user)
{
    /**
     * @brief
     * The room isn't created if :
     *  - its name is "Lobby"
     *  - user is already in a room
     */
    if (name.compare("Lobby") != 0 && user->getRoomName().compare("Lobby") == 0)
    {
        this->roomList.push_back(new Room(name, user));
        return true;
    }
    return false;
};

void Lobby::removeRoom(Room *room)
{
    for (size_t i = 0; i < roomList.size(); i++)
    {
        if (roomList[i]->getName() == room->getName())
        {
            roomList.erase(roomList.begin() + i);
            break;
        }
    }
}

Room *Lobby::findRoom(int id)
{
    for (size_t i = 0; i < this->roomList.size(); i++)
    {
        if (this->roomList[i]->getID() == id)
            return this->roomList[i];
    }
    return nullptr;
}

Room *Lobby::findRoomByName(string name)
{
    for (size_t i = 0; i < this->roomList.size(); i++)
    {
        if (name.compare(this->roomList[i]->getName()) == 0)
            return this->roomList[i];
    }
    return nullptr;
}

string Lobby::getRoomListToString()
{
    string s = "";
    s += "\u001b[38;5;209mROOM LIST\u001b[0m\n";
    for (size_t i = 0; i < this->roomList.size(); i++)
    {
        s += "- " + this->roomList[i]->getName() + "\n";
    }
    return s;
}

string Lobby::getUserListToString()
{
    string s = "";
    s += "\u001b[38;5;209mUSER LIST\u001b[0m\n";
    for (size_t i = 0; i < this->userList.size(); i++)
    {
        s += "- " + this->userList[i]->getUserName() + "\n";
    }
    return s;
}

int Lobby::launch(StreamSocket *socket)
{
    Communication output;
    thread threadReader;

    // Read Client Request (debug)
    output.setReader(socket->getSockfd());
    output.setWriter(1);
    threadReader = std::thread(serverReadEveryone, std::ref(output));

    threadReader.join();
    return 0;
};

void Lobby::disconnectUser(int id)
{
    for (size_t i = 0; i < userList.size(); i++)
    {
        if (userList[i]->getID() == id)
        {
            userList[i]->disconnect();
            if (userList[i]->getRoom() != nullptr)
            {
                Room *room = userList[i]->getRoom();
                room->disconnect(userList[i]);
                if (room->size() == 0)
                {
                    this->removeRoom(room);
                }
            }
            userList.erase(userList.begin() + i);
            cout << "[-] Client" << to_string(id) << " has logged out" << endl;
            break;
        }
    }
}

int socketConnect(Lobby *lobby, string ip, int port)
{
    lobby->setSock(new StreamSocket(ip, port));

    int err = lobby->getSock()->connect();
    if (err != 0)
    {
        delete lobby->getSock();
        perror("[-]Error in connection: ");
        return (err);
    }
    
    cout << "Server : [+] Lobby is connected" << endl;
    return 0;
}

void launchLobby(Lobby *lobby, StreamSocket *socket)
{
    lobby->launch(socket);
}

void serverReadEveryone(Communication &param)
{

    char buffer[1024] = {0};
    cout << "Lobby : " << param.getReader() << "-" << param.getWriter() << endl;
    
    while (true)
    {
        
        if (read(param.getReader(), &buffer, 1024)){
            cout << "Server Reader Failed !" << endl;
            break;
        }
            
    }

    return;
}

Lobby::~Lobby()
{
    for (size_t i = 0; i < userList.size(); i++)
    {
        delete userList[i];
    }
    for (size_t i = 0; i < roomList.size(); i++)
    {
        delete roomList[i];
    }
    t->join();
    delete this->sock;
    return;
}
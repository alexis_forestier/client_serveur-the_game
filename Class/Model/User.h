#ifndef USER_H
#define USER_H

#include <iostream>
#include <thread>
#include <string>
#include "../request.h"
#include "Lobby.h"

using namespace std;

class Room;

class User{
    private:
    int reader;     //its socket number
    int writer;     //server
    Request r_user;

    bool run;
    std::thread t;

    int id;

    int room_id;
    string room_name;
    Room* room;
    string user_name;

    public:
    User(int reader, int writer, int id, Lobby* lobby);

    void setID(int id){
        this->id = id;
    }

    int getID(){
        return this->id;
    }

    string getStringID(){
        return to_string(this->id);
    }

    void setRoom(Room* room){
        this->room = room;
    }

    Room* getRoom(){
        return this->room;
    }


    void setRoomID(int room){
        this->room_id = room;
    }

    int getRoomID(){
        return this->room_id;
    }

    void setRoomName(string name){
        this->room_name = name;
    }

    string getRoomName(){
        return this->room_name;
    }

    string getRoomNameEffect(){
        if(this->room_name.compare(string("Lobby")) == 0) 
            return "\u001b[92mLobby\u001b[0m";
        else
            return "Room(\u001b[95m"+this->room_name+"\u001b[0m)";
    }

    void setUserName(string name){
        this->user_name = name;
    }

    string getUserName(){
        return this->user_name;
    }

    int getReader(){return reader;}
    Request& getTalk(){return r_user;}

    bool getRun(){return this->run;}

    void disconnect(){
        run = false;
    }

    ~User(){
        t.join();
    }
};

#endif //USER_H
#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <string>
#include <deque>
#include <stdlib.h>
#include <time.h>

using namespace std;

class User;
class Game;

class Room
{
private:
    int id;
    string name;
    deque<User *> users_list;

    bool is_playing;
    Game *game;

public:
    Room(string name, User *user)
    {
        srand(time(NULL));
        this->id = rand() % 9999 + 1;

        this->name = name;
        user->setRoom(this);
        user->setRoomID(this->id);
        user->setRoomName(name);
        this->users_list.push_back(user);
        this->is_playing = false;
    }

    int getID() { return this->id; }
    string getName() { return this->name; }

    void addUser(User *user);

    void removeUser(User *user);

    void play();

    void alert(string alert);

    int size() { return users_list.size(); }

    void setIsPlaying(bool isPlaying)
    {
        this->is_playing = isPlaying;
    }

    bool isPlaying()
    {
        return this->is_playing;
    }

    bool setPile(deque<int> &cards, int card_pos, int pile_number);

    string cardsToString(deque<int> &player_cards);

    string getInfo();

    string getUserListToString();

    void disconnect(User* user){
        this->removeUser(user);
        user->setRoom(nullptr);
        user->setRoomID(0);
        user->setRoomName("Lobby");
    }

    ~Room(){
        
        for (size_t i = 0; i < users_list.size(); i++)
        {
            delete this->users_list[i];
        }
        
    }
};

#endif // ROOM_H
#include <sys/socket.h>
#include <unistd.h>
#include <iostream>

#include <string.h>

#include "User.h"
#include "Room.h"

#include "Game.h"

void Room::addUser(User *user)
{
    if (!is_playing)
    {
        user->setRoom(this);
        user->setRoomID(this->id);
        user->setRoomName(this->name);
        this->alert(user->getUserName() + " joined the room ! (" + to_string(this->users_list.size() + 1) + " player" + (this->users_list.size() + 1 > 1 ? "s" : "") + ")");
        this->users_list.push_back(user);
    }
}

void Room::removeUser(User *user)
{
    for (size_t i = 0; i < users_list.size(); i++)
    {
        if (users_list[i]->getID() == user->getID())
        {
            users_list[i]->setRoom(nullptr);
            users_list[i]->setRoomName("Lobby");
            users_list.erase(users_list.begin() + i);
            this->alert(user->getUserName() + " has left the room ! (" + to_string(this->users_list.size()) + " player" + (this->users_list.size() > 1 ? "s" : "") + ")");
            break;
        }
    }
}

void Room::play()
{
    if (!is_playing)
    {
        this->is_playing = true;
        this->game = new Game(this->users_list, this);
    }
}

void Room::alert(string alert)
{
    string str_alert = string("\u001b[35m[" + this->getName() + "]\u001b[33m " + alert + "\u001b[0m\n");
    const char *alertRoom = str_alert.data();

    for (size_t i = 0; i < this->users_list.size(); i++)
    {
        if (write(this->users_list[i]->getReader(), alertRoom, strlen(alertRoom)) <= 0)
        {
            cout << "The alert message for " << this->users_list[i]->getUserName() << " failed" << endl;
        }
    }
}

bool Room::setPile(deque<int> &cards, int card_pos, int pile_number)
{
    if (card_pos > (int)cards.size() - 1)
        return 0;

    int card = cards[card_pos];
    if (card >= 2 || card <= 99)
    {

        if (pile_number == 1)
        {
            if (card > game->pile1 || card == game->pile1-10) // ascending
            {
                game->pile1 = card;
                cards.erase(cards.begin() + card_pos);
                return 1;
            }
        }
        else if (pile_number == 2)
        {
            if (card > game->pile2 || card == game->pile2-10) // ascending
            {
                game->pile2 = card;
                cards.erase(cards.begin() + card_pos);
                return 1;
            }
        }
        else if (pile_number == 3)
        {
            if (card < game->pile3 || card == game->pile3+10) // descending
            {
                game->pile3 = card;
                cards.erase(cards.begin() + card_pos);
                return 1;
            }
        }
        else if (pile_number == 4)
        {
            if (card < game->pile4 || card == game->pile4+10) // descending
            {
                game->pile4 = card;
                cards.erase(cards.begin() + card_pos);
                return 1;
            }
        }
    }

    return 0;
}

string Room::cardsToString(deque<int> &player_cards)
{
    return this->game->getPileToString(player_cards);
}

string Room::getInfo()
{
    string s = "";

    s += "\u001b[38;5;209mROOM INFO\u001b[0m : Room(\u001b[95m" + this->getName() + "\u001b[0m) ID(\u001b[92m#" + to_string(this->getID()) + "\u001b[0m)\n";
    s += "\u001b[38;5;209mPILE SIZE\u001b[0m : " + to_string(this->game->cardsPileSize()) + "\u001b[0m\n";
    s += "\u001b[38;5;209mGAME\u001b[0m\n";
    s += "\u001b[36m 1:\u001b[33m" + to_string(this->game->pile1) + "\u001b[36m 3:\u001b[33m" + to_string(this->game->pile3) + "\u001b[0m\n";
    s += "\u001b[36m 2:\u001b[33m" + to_string(this->game->pile2) + "\u001b[36m 4:\u001b[33m" + to_string(this->game->pile4) + "\u001b[0m\n";

    return s;
}

string Room::getUserListToString()
{
    string s = "";
    s += "\u001b[38;5;209mUSER IN THIS ROOM\u001b[0m\n";
    for (size_t i = 0; i < this->users_list.size(); i++)
    {
        s += "- " + this->users_list[i]->getUserName() + "\n";
    }
    return s;
}

#include "Game.h"

#include <unistd.h>
#include <iostream>
#include <string.h>

#include "User.h"
#include "Room.h"
#include "../../Server/Protocol/protocol.h"

Game::Game(deque<User *> players_list, Room *room)
{
    this->players = players_list;
    this->room = room;

    new std::thread(playGame, this);
};

void Game::initCardsPile()
{
    for (int i = 2; i <= 99; i++)
    {
        cardsPile.push_back(i);
    }
}

deque<int> Game::getCards(int n)
{
    srand(time(NULL));
    deque<int> pile;

    for (int i = 0; i < n; i++)
    {

        int random = rand() % this->cardsPile.size();

        pile.push_back(this->cardsPile[random]);
        this->cardsPile.erase(this->cardsPile.begin() + random);
    }

    return pile;
}

void Game::pickCards(deque<int> &player_cards)
{
    srand(time(NULL));
    deque<int> pile;

    while ((int)player_cards.size() < this->nb_cardByPlayer && (int)this->cardsPile.size() > 0)
    {

        int random = rand() % this->cardsPile.size();

        player_cards.push_back(this->cardsPile[random]);
        this->cardsPile.erase(this->cardsPile.begin() + random);
    }
}

deque<Player> Game::initPlayers()
{
    deque<Player> players_list;
    int nb_players = this->players.size();
    int nb_cardsByPlayers = 0;

    if (nb_players > 2)
    {
        nb_cardsByPlayers = 6;
    }
    else if (nb_players == 2)
    {
        nb_cardsByPlayers = 7;
    }
    else
    {
        nb_cardsByPlayers = 8;
    }

    for (int i = 0; i < nb_players; i++)
    {
        players_list.push_back(Player{this->players[i], this->getCards(nb_cardsByPlayers), false});
    }

    this->nb_cardByPlayer = nb_cardsByPlayers;

    return players_list;
}

string Game::getPileToString(deque<int> &player_cards)
{
    string str = "{";
    for (size_t i = 0; i < player_cards.size(); i++)
    {
        str += "\u001b[36m" + to_string(i) + ":\u001b[0m";
        str += to_string(player_cards[i]);
        if (i != player_cards.size() - 1)
        {
            str += ", ";
        }
    }
    str += "}";
    return str;
}

void Game::sendData(deque<Player> players)
{

    for (size_t i = 0; i < players.size(); i++)
    {
        string str_data = string("\u001b[35m[" + this->getRoom()->getName() + "]\u001b[33;1m THE GAME (round : " + to_string(this->round) + ")\u001b[0m\n");
        str_data += string("\u001b[36m 1:\u001b[33m" + to_string(this->pile1) + "\u001b[36m 3:\u001b[33m" + to_string(this->pile3) + "\u001b[0m\n");
        str_data += string("\u001b[36m 2:\u001b[33m" + to_string(this->pile2) + "\u001b[36m 4:\u001b[33m" + to_string(this->pile4) + "\u001b[0m\n");
        str_data += string("\u001b[33m Your cards pile : \u001b[0m" + this->getPileToString(players[i].cards) + "\n");

        const char *chr_data = str_data.data();

        if (write(players[i].player->getReader(), chr_data, strlen(chr_data)) <= 0)
        {
            cout << "The data message for " << players[i].player->getUserName() << " failed" << endl;
        }
    }
}

void playGame(Game *game)
{
    // init the game

    game->pile1 = 1, game->pile2 = 1, game->pile3 = 100, game->pile4 = 100;

    game->initCardsPile();

    deque<Player> players = game->initPlayers();

    char buffer[1024] = {0};

    //bool win = false;
    bool conf = 0;
    game->round = 1;
    // int user_turn = 0;
    int min_pose = 2;
    int j = 0;
    string msg = "";

    game->getRoom()->alert("The game started !");

    while (game->gameover(players) == 0)
    {
        game->sendData(players);

        for (size_t i = 0; i < players.size(); i++)
        {
            if (!players[i].lose)
            {
                for (j = 0; (!conf || j < min_pose) && players[i].cards.size() > 0;)
                {
                    int err = read(players[i].player->getReader(), &buffer, _REQUEST_LIMIT_);
                    if (err < 0)
                    {
                        cout << players[i].player->getUserName() << " : Game Reading error" << endl;
                        players[i].lose = true;
                        break;
                    }
                    else if (err > 0)
                    {
                        gameProtocol(buffer, players[i].player->getReader(), game->getRoom(), players[i].cards, conf, j);
                        for (int i = 0; i < 1024; i++)
                        {
                            buffer[i] = '\0';
                        }
                    }
                }
            }

            game->pickCards(players[i].cards);
            msg = players[i].player->getUserName() + " draws cards !";

            cout << j <<endl;
            if ((game->playerLose(players[i]) || j < min_pose) && game->gameover(players) == 0)
            {
                players[i].lose = true;
                msg = players[i].player->getUserName() + " lose the game !";
            }

            game->getRoom()->alert(msg);
            conf = false;

            min_pose = game->cardsPileSize() == 0 ? 1 : 2;
        }

        game->round++;
    }

    game->getRoom()->alert((game->gameover(players) == 1 ? "Players wins the game !" : "Game wins the game !"));
    game->getRoom()->setIsPlaying(false);

    delete game;
}

int Game::gameover(deque<Player> players)
{
    int nb_lose = 0;
    int nb_cardsEmpty = 0;

    for (size_t i = 0; i < players.size(); i++)
    {
        if ((int)players[i].cards.size() == 0)
        {
            nb_cardsEmpty++;
        }
        if ((int)players[i].lose == true)
        {
            nb_lose++;
        }
    }

    // Game win
    if (nb_lose == (int) players.size())
    {
        return 2;
    }

    // Players win
    if (nb_cardsEmpty == (int) players.size())
    {
        return 1;
    }

    return 0;
}

bool Game::playerLose(Player player)
{

    for (size_t i = 0; i < player.cards.size(); i++)
    {
        int card = player.cards[i];

        if (card > this->pile1 || card == this->pile1 - 10) // ascending
        {
            return 0;
        }

        if (card > this->pile2 || card == this->pile2 - 10) // ascending
        {
            return 0;
        }

        if (card < this->pile3 || card == this->pile3 + 10) // descending
        {
            return 0;
        }

        if (card < this->pile4 || card == this->pile4 + 10) // descending
        {
            return 0;
        }
    }

    return 1;
}

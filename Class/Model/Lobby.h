#ifndef LOBBY_H
#define LOBBY_H

#include <iostream>
#include <thread>
#include <deque>

#include <sys/time.h>
#include "../socket.h"
#include "../Communication.h"

using namespace std;
using namespace stdsock;

class User;
class Room;

class Lobby
{
private:
    string ip;
    int port;
    std::thread *t;
    StreamSocket *sock;

    Communication com_server;
    deque<User *> userList;
    deque<Room *> roomList;

public:
    // Constructor

    Lobby(){};
    Lobby(string ip, int port);

    // IP

    string getIP(){ return this->ip;}
    int getPort(){ return this->port;}

    // Socket

    void setSock(StreamSocket* sock){this->sock = sock;}
    StreamSocket* getSock(){return this->sock;}



    void setThread(std::thread *t){this->t = t;}
    std::thread* getThread(){return this->t;}
    
    void set(int reader)
    {
        this->com_server.setReader(reader);
        cout << "Server : [+] Lobby is connected" << endl;
    }

    int getReader() { return com_server.getReader(); }
    Communication getCom() { return com_server; }
    int launch(StreamSocket *socket); 
    

    // User functions

    void addUser(User *user);
    void removeUser(int i);
    int size() { return userList.size(); }
    string getUserListToString();
    void disconnectUser(int id);
    
    // Room functions

    bool addRoom(string name, User* user);
    void removeRoom(Room *room);
    Room* findRoom(int id);
    Room* findRoomByName(string name);
    int roomListSize() const {return (int) this->roomList.size();}
    string getRoomListToString();

    // Destructor

    ~Lobby();
    
};

void launchLobby(Lobby *lobby, StreamSocket *socket);

int socketConnect(Lobby *lobby, string ip, int port);

void serverReadEveryone(Communication &param);

#endif // LOBBY_H
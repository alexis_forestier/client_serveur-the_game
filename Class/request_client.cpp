#include <sys/socket.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include "request.h"

using namespace std;

/**
 * @brief
 * Communication method for client
 * @param param
 */
void doRequest(Request &param, bool &run)
{

    char buffer[_REQUEST_LIMIT_] = {0};

    cout << "request : " << param.getReader() << "-" << param.getWriter() << endl;

    while (run == true)
    {
        if (read(param.getReader(), &buffer, _REQUEST_LIMIT_) <= 0)
        {
            cout << "Reading error" << endl;
            break;
        }


        if (string(buffer).compare(string("\u001b[34;1m[Server]\u001b[0m logout")) == 0)
        {
            cout << "fin" << endl;
            break;
        }

        if (write(param.getWriter(), buffer, strlen(buffer)) <= 0)
        {

            cout << "Writting error" << endl;
            break;
        }
        else
        {
            for (int i = 0; i < _REQUEST_LIMIT_; i++)
            {
                buffer[i] = '\0';
            }
        }
    }

    return;
}

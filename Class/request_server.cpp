#include <sys/socket.h>
#include <unistd.h>
#include <iostream>
#include <string.h>

#include "request.h"
#include "../Server/Protocol/protocol.h"
#include "Model/Lobby.h"
#include "Model/User.h"

using namespace std;

/**
 * @brief
 * Communication method for client in Server side
 * @param param
 * @param user
 */
void request(Request &param, User* user, Lobby* lobby)
{

    char buffer[_REQUEST_LIMIT_] = {0};

    cout << "[Client" << user->getID() << "] req : " << param.getReader() << "-" << param.getWriter() << endl;

    while(user->getRun() == true)
    {
          
        if (read(param.getReader(), &buffer, _REQUEST_LIMIT_) < 0)
        {
            cout << "Reading error" << endl;
            break;
        }
        

        int err = write(param.getWriter(), buffer, strlen(buffer));
        if (err < 0)
        {
            cout << "Writting error" << endl;
            break;
        }
        else if(err > 0)
        {

            protocol(buffer, param.getReader(), lobby, user);
            for (int i = 0; i < _REQUEST_LIMIT_; i++)
            {
                buffer[i] = '\0';
            }
        }
        
        
    }

    lobby->disconnectUser(user->getID());

    return;
}
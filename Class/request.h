#ifndef REQUEST_H
#define REQUEST_H

class User;
class Lobby;

const int _REQUEST_LIMIT_ = 1024;

class Request
{
private:
    int readfd;
    int writefd;
public:
    Request(){}
    Request(Request &request){
        this->readfd = request.getReader();
        this->writefd = request.getWriter();
    }
    void set(int readfd, int writefd){
        this->readfd = readfd;
        this->writefd = writefd;
    }
    int getReader() const { return readfd;}
    int getWriter() const { return writefd;}
    void setReader(int sockfd) {
        readfd=sockfd;
    }
    void setWriter(int sockfd) {
        writefd=sockfd;
    }
    ~Request(){}
};

/**
 * @brief 
 * Method only for the client side
 * @param param 
 */
void doRequest(Request &param, bool &run);

/**
 * @brief 
 * Method only for the client connection in server side
 * @param param 
 * @param user 
 * @param lobby
 */
void request(Request &param, User* user, Lobby* lobby);


#endif /* REQUEST_H */
